#include <SPI.h>       //librairie pour synchroniser la communication
#include <Ethernet.h>

// NETWORK CONFIGURATION 
byte mac[] = { 
  0x90, 0xA2, 0xDA, 0x0F, 0x8B, 0x19 };   //adresse mac physique
byte ip[] = { 
  192, 168, 0, 1 };                       // IP dans le LAN
byte gateway[] = { 
  192, 168, 0, 100 };                   // DG
byte subnet[] = { 
  255, 255, 255, 0 };                  //mask de sous-réseau
EthernetServer server(80);                             //port du serveur (HTTP port 80)


// Server IP Target
char serverTarget[] = "192.168.00.100"; 

// data receive from GET HTTP Protocol 
char licencee_plate_status_validation; 
// IF status_receive = 0 => license palte not accepeted 
// IF status_receive =  => 1 licnse plate accepeted

const int CapteurSortie=2;
const int CommandeOuverture=8;
int FM=0;  //bit pour détecter un front montant

void setup()
{
  Serial.begin(9600);

  pinMode(CapteurSortie,INPUT);
  pinMode(CommandeOuverture,OUTPUT);


  //démarre la connection Ethernet et le serveur

  Ethernet.begin(mac, ip, gateway, subnet);
  server.begin();
  Serial.print("l'adresse IP du serveur est: ");
  Serial.println(Ethernet.localIP());
}

void loop()
{
  EthernetClient client = server.available();

  // Hello from arduino => check if Arduino is alive 
  hello(client); 
  // Check if a car goin out 
  checkExitSensor(client);
  if (client)  //si un client est connecté
  {
    if(client.connected())        // Tant que le client est connecté
    {
      if(client.available())         // A-t-il des choses à dire ?
      {
        readDataFromClient(client); 
      }
    }
  }  
  delay(10);
}

// ================================= METHODES 

void readDataFromClient(EthernetClient client){

  // send HTTP header to client : 
  client.println(F("HTTP/1.1 200 OK"));
  client.println(F("Content-Type: text/html"));
  client.println();

  // Read GET CALL for know Status of licence plate 
  for (int i = 0; i < 6; i++) {
    if (i == 5) {
      licencee_plate_status_validation = client.read();
      Serial.println(licencee_plate_status_validation); 
      openParking(licencee_plate_status_validation); 
    } 
    else {
      client.read();
    }
  }
  client.stop();
}

void openParking(char licencee_plate_status_validation)
{
  if (licencee_plate_status_validation == '1')
  {      
    Serial.println("oepnParking true  : " );
    Serial.println(licencee_plate_status_validation);    
    //on ouvre la barrière pour faire entrer la voiture
    digitalWrite(CommandeOuverture,HIGH);
    delay(1500);
    digitalWrite(CommandeOuverture,LOW);
  }
}

void checkExitSensor(EthernetClient client)
{
  if(CapteurSortie==HIGH)
  {

    exitRequest(client);

    if(FM==0)
    {
      // CONTACT CLIENT by HTTP GET method  
      exitRequest(client);

      //on ouvre la barrière pour faire sortir la voiture
      digitalWrite(CommandeOuverture,HIGH);
      delay(1500);
      digitalWrite(CommandeOuverture,LOW);
      FM=1;
    }
  }
  else //donc si CapteurSortie =LOW
  {
    FM=0;
  }
}


void exitRequest(EthernetClient client){
  if (client.connect(serverTarget, 3000)) {
    // Make a HTTP request:
    client.println("GET /carDetected HTTP/1.1");
    client.println("Host: 192.168.0.100");
    client.println("Connection: close");
    client.println();
  } 
  else {
    // if you didn't get a connection to the server:
    Serial.println("connection failed from exit Request");
  }
}


void hello(EthernetClient client){
  if (client.connect(serverTarget, 3000)) {
    Serial.println("connected");
    // Make a HTTP request:
    client.println("GET /hello HTTP/1.1");
    client.println("Host: 192.168.0.100");
    client.println("Connection: close");
    client.println();
  } 
  else {
    // if you didn't get a connection to the server:
    Serial.println("connection failed from Hello");
  }
}


